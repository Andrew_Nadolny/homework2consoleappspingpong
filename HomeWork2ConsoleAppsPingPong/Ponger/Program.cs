﻿using System;
using System.Text;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            ListenerQueue listenerQueue = new ListenerQueue("pong_queue", "ping_queue");
            listenerQueue.ListenQueue("pong");
            Console.ReadLine();
        }
    }
}
