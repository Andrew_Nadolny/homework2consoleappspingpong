﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumer : IMessageConsumer
    {
        public readonly MessageComsumerSettings _settings;
        public readonly EventingBasicConsumer _consumer;

        public event EventHandler<BasicDeliverEventArgs> Receiver
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }

        public MessageConsumer(MessageComsumerSettings settings)
        {
            _settings = settings;
            _consumer = new EventingBasicConsumer(_settings.Channel);
        }

        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            if (processed)
            {
                _settings.Channel.BasicAck(deliveryTag, false);
            }
            else
            {
                _settings.Channel.BasicNack(deliveryTag, false, true);
            }
        }

        public void Connect()
        {
            _settings.Channel.BasicConsume(_settings.QueueName, _settings.AutoAcknowledge, _consumer);
        }
    }
}
