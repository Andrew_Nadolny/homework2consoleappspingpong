﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageQueue : IMessageQueue
    {
        public readonly IConnection _connection;
        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }
        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
            : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchangeName, messageScopeSettings.ExchangeType);

            if(messageScopeSettings.QueueName != null)
            {
                BindQueue(messageScopeSettings.ExchangeName, messageScopeSettings.RoutingKey, messageScopeSettings.QueueName);
            }
        }
        public IModel Channel { get; protected set; }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            if(!string.IsNullOrEmpty(exchangeName) || !string.IsNullOrEmpty(exchangeType))
            {
                Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
            }
        }
        public void BindQueue(string exchangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, false, false, false);
            if (!string.IsNullOrEmpty(exchangeName))
            {
                Channel.QueueBind(queueName, exchangeName, routingKey);
            }
        }
        public void Dispose()
        {
            Channel?.Dispose();
            _connection.Dispose();
        }
    }
}
