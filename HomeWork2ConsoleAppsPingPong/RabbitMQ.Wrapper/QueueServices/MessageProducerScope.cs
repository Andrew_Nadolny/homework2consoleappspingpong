﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducerScope : IMessageProducerScope
    {
        public readonly Lazy<IMessageQueue> _messageQueueLazy;
        public readonly Lazy<IMessageProducer> _messageProducerLazy;

        public readonly MessageScopeSettings _messageScopeSettings;
        public readonly IConnectionFactory _connectionFactory;

        public MessageProducerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue());
            _messageProducerLazy = new Lazy<IMessageProducer>(CreateMessageProduces());
        }
        public IMessageProducer MessageProducer => _messageProducerLazy.Value;
        public IMessageQueue MessageQueue => _messageQueueLazy.Value;

        public IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }

        public IMessageProducer CreateMessageProduces()
        {
            return new MessageProducer(new MessageProducerSettings{ 
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    _messageScopeSettings.ExchangeType,
                    _messageScopeSettings.ExchangeName,
                    _messageScopeSettings.RoutingKey
                    )
            });
        }
        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
