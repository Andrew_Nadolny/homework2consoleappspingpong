﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class SenderMessageToQueue
    {
        private readonly IMessageProducerScope _messageProducerScope;

        public SenderMessageToQueue(string queue)
        {
            _messageProducerScope = new MessageProducerScopeFactory(new ConnectionFactory() { HostName= "localhost" })
                .Open(new MessageScopeSettings
                {
                    ExchangeName = "",
                    ExchangeType = "",
                    QueueName = queue,
                    RoutingKey = queue
                });
        }
        public void SendMessageToQueue(string message)
        {
            _messageProducerScope.MessageProducer.Send(message);
        }
    }
}
