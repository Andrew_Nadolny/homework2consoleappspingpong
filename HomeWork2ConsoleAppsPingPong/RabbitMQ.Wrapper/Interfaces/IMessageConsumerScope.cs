﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScope
    {
        IMessageConsumer MessageConsumer { get; }
        IMessageQueue MessageQueue { get; }
        IMessageQueue CreateMessageQueue();
        IMessageConsumer CreateMessageConsumer();
        void Dispose();
    }
}
