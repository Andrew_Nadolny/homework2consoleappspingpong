﻿using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScopeFactory
    {
        public IMessageProducerScope Open(MessageScopeSettings messageScopeSettings);
    }
}
