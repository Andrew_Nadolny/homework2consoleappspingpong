﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Receiver;
        void SetAcknowledge(ulong deliveryTag, bool processed);
        void Connect();
    }
}
