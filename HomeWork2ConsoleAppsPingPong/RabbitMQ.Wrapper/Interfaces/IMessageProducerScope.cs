﻿using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScope
    {
        public IMessageProducer MessageProducer { get; }
        public IMessageQueue MessageQueue { get; }
        public IMessageQueue CreateMessageQueue();
        public IMessageProducer CreateMessageProduces();
        public void Dispose();
    }
}
