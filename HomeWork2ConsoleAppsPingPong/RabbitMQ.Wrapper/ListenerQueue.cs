﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class ListenerQueue
    {
        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly IMessageProducerScope _messageProducerScope;
        private string _responceMessage;

        public ListenerQueue(string queue, string responceQueue)
        {
            _messageProducerScope = new MessageProducerScopeFactory(new ConnectionFactory() { HostName = "localhost" })
                .Open(new MessageScopeSettings
                {
                    ExchangeName = "",
                    ExchangeType = "",
                    QueueName = responceQueue,
                    RoutingKey = responceQueue
                });
            _messageConsumerScope = new MessageConsumerScopeFactory(new ConnectionFactory() { HostName = "localhost" })
                .Connect(new MessageScopeSettings
                {
                    ExchangeName = "",
                    ExchangeType = "",
                    QueueName = queue,
                    RoutingKey = queue
                });
        }
        public void ListenQueue(string responceMessage)
        {
            _responceMessage = responceMessage;
            _messageConsumerScope.MessageConsumer.Receiver += GetValue;
        }

        public void GetValue(object sender, BasicDeliverEventArgs basicDeliverEventArgs)
        {
            var body = basicDeliverEventArgs.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine("{0}: {1}", DateTime.Now, message);
            System.Threading.Thread.Sleep(2500);
            _messageConsumerScope.MessageConsumer.SetAcknowledge(basicDeliverEventArgs.DeliveryTag, true);
            _messageProducerScope.MessageProducer.Send(_responceMessage);
        }
    }
}
