﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Text;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            SenderMessageToQueue senderMessageToQueue = new SenderMessageToQueue("pong_queue");
            senderMessageToQueue.SendMessageToQueue("ping");
            ListenerQueue listenerQueue = new ListenerQueue("ping_queue", "pong_queue");
            listenerQueue.ListenQueue("ping");
            Console.ReadLine();

        }
    }
}
